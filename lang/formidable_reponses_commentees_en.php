<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/formidable_reponses_commentees?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'commentaire_apres_reponse_explication' => 'Enter a comment that will be provided to the user after he sent his reply. You can use the shortcuts of SPIP.',
	'commentaire_apres_reponse_label' => 'Comment to display after the answer'
);
