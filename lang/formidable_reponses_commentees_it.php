<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/formidable_reponses_commentees?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'commentaire_apres_reponse_explication' => 'Inserisci un commento che verrà fornito all’utente dopo aver inviato la risposta. È possibile utilizzare le scorciatoie SPIP.',
	'commentaire_apres_reponse_label' => 'Commento da visualizzare dopo la risposta'
);
