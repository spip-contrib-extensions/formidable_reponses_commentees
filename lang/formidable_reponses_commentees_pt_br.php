<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/formidable_reponses_commentees?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'commentaire_apres_reponse_explication' => 'Informe um comentário que será mostrado ao usuário após ele ter enviado a sua resposta. Você pode usar os atalhos do SPIP.',
	'commentaire_apres_reponse_label' => 'Comentário a exibir após a resposta'
);
